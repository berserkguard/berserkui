#pragma once

#include <unordered_map>
#include <list>
#include <memory>

#include <SFML/Graphics.hpp>

#include "Widget.h"
#include "Theme.h"

/**
 * @namespace Berserk
 * The Berserk namespace is prefixed to everything accessible from this library to prevent naming
 * collisions.
 */
namespace Berserk {
	/**
	 * A class for grouping together & managing widgets.
	 * @note There can be multiple UIs. One possible use is to create a layered UI system.
	 *       For example, if you want a player's action bar to always display above their
	 *       inventory, skills window, quest log, etc., you could create a primaryUI and a
	 *       secondaryUI, and in your game's rendering code, render the primaryUI after the
	 *       secondaryUI. This way, every widget of the secondaryUI will always be below every
	 *       widget of the primaryUI. Another use of multiple UIs is to easily integrate
	 *       BerserkUI into a game that has multiple scenes: you can create a different UI for
	 *       each scene, and choose when to render each UI based on the current scene.
	 */
	class UI {
	public:
		UI();

		/**
		 * Adds a named widget to the UI.
		 * @param name The name for the widget. Must be unique.
		 * @param widget The widget to add to the UI.
		 * @return Returns the input widget for convenience.
		 */
		WidgetPtr addWidget(const std::string& name, WidgetPtr widget);

		/**
		 * Returns the widget with the given name. Returned as a WidgetPtr by default.
		 * @param name The name of the widget.
		 * @return A widget if found, else nullptr.
		 */
		template <typename T = Berserk::Widget>
		std::shared_ptr<T> getWidget(const std::string& name) {
			if (widgetsMap.find(name) != widgetsMap.end()) {
				return std::dynamic_pointer_cast<T>(widgetsMap.at(name));
			}
			return nullptr;
		}

		/**
		 * Brings the specified widget to the front of list. Widgets at the front of the
		 * list render on top of everything else and receive events and updates first.
		 * @param widget The widget to bring to the front.
		 */
		void bringToFront(WidgetPtr widget);

		/**
		 * Renders the UI by iteratively rendering each of the widgets. Widgets are
		 * iterated over in their order in the UI (so the widget at the front of the
		 * list will be rendered on top of all the others).
		 * @note The UI must be visible (via setVisible()) in order to be rendered.
		 * @param target The SFML RenderTarget to render to.
		 */
		void render(sf::RenderTarget& target);

		/**
		 * Updates the UI by iteratively updating each of the widgets. Widgets are
		 * iterated over in their order in the UI.
		 * @param timestep The time passed (in seconds) since the last update.
		 */
		void update(double timestep);

		/**
		 * Handles an sf::Event by iteratively handling it on each of the widgets. If a
		 * widget's handleEvent returns true, the remaining widgets are skipped. This is
		 * because that widget requested to stop propagation of the event. Widgets are
		 * iterated over in their order in the UI.
		 * @param event The sf::Event to process. Should come from sf::Window::pollEvent().
		 * @return True if the UI requests event propagation to stop (for example, an opaque
		 * 	       window was clicked). Under most circumstances, you should ignore an event
		 *         if this returns true.
		 */
		bool handleEvent(const sf::Event& event);

		/**
		 * Sets the visibility of the UI. Supercedes widget-level visibility.
		 * @param visible Set to true to enable rendering the UI, false to disable.
		 */
		void setVisible(bool visible);

		/**
		 * Returns whether the UI is visible.
		 * @return True if the UI is visible, false if it is not.
		 */
		bool isVisible();

		void setTheme(Theme& theme);
	private:
		Theme* theme;

		/** For toggling the visibility of the UI. */
		bool visible;

		/** For keeping track of the render order of the widgets. */
		std::list<WidgetPtr> widgetsList;

		/** For getting widgets by name in constant time. */
		std::unordered_map<std::string, WidgetPtr> widgetsMap;
	};
}
