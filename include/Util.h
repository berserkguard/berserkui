#pragma once

namespace Berserk {
	/**
	 * @namespace Berserk::Util
	 * A grouping of helper functions that are touched by a large part of BerserkUI.
	 * You won't ever need to use these yourself in order to use BerserkUI, but you
	 * are more than welcome to if you so desire.
	 */
	namespace Util {
		/**
		 * Checks whether the given point is inside the rectangle.
		 * @param point An sf::Vector2 representing the point.
		 * @param rect An sf::Rect representing the rect.
		 * @return True if the point is inside or on the rect, else false.
		 */
		template <typename T>
		bool isPointInRect(sf::Vector2<T> point, sf::Rect<T> rect) {
			bool xCollision = point.x >= rect.left && point.x <= rect.left + rect.width;
			bool yCollision = point.y >= rect.top && point.y <= rect.top + rect.height;
			return xCollision && yCollision;
		}
	}
}
