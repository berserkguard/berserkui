#pragma once

#include <SFML/Graphics.hpp>

namespace Berserk {
	class Theme {
	public:
		virtual const sf::Texture& getTexture(const std::string& widgetClassName) = 0;
		virtual bool hasWidget(const std::string& widgetClassName) = 0;
	};
}
