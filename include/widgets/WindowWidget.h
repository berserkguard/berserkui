#pragma once

#include <memory>

#include <SFML/Graphics.hpp>

#include "Widget.h"

namespace Berserk {
	/**
	 * The base class for all windows.
	 */
	class WindowWidget : public Widget {
	public:
		WindowWidget();

		void render(sf::RenderTarget& target) override;
		void update(double timestep) override;
		bool handleEvent(const sf::Event& event) override;

		void setDraggable(bool draggable);
		bool isDraggable();

		std::string getClassName() override;
	private:
		bool handleLeftMouseButtonPressed(const sf::Event& event);
		bool handleLeftMouseButtonReleased(const sf::Event& event);
		bool handleMouseMoved(const sf::Event& event);

		sf::Vector2f mousePos;
		sf::Vector2f dragAnchor;

		bool draggable;
		bool dragInitialized, dragging;
	};
	typedef std::shared_ptr<Berserk::WindowWidget> WindowWidgetPtr;
}
