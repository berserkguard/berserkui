#include "FileTheme.h"

using namespace Berserk;

FileTheme::FileTheme(const std::string& path) {
	this->path = path;
}

const sf::Texture& FileTheme::getTexture(const std::string& widgetClassName) {
	if (!hasWidget(widgetClassName)) {
		sf::Texture tex;
		tex.loadFromFile(path + widgetClassName + ".png");
		textures.insert(std::pair<std::string, sf::Texture>(widgetClassName, tex));
	}
	return textures.at(widgetClassName);
}

bool FileTheme::hasWidget(const std::string& widgetClassName) {
	return textures.count(widgetClassName) > 0;
}

